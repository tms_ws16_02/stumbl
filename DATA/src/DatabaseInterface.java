/**
 * Created by regrau on 21.12.16.
 */
public interface DatabaseInterface {
   void insertPerson(DataRow row);
   void insertCoordinates(DataRow row);
   void insertDeportationsziel(DataRow row);
   void insertArea(DataRow row);
   void insertAddress(DataRow row);
   void insertDeporation(DataRow row);
   void insertData(DataRow row);
}
