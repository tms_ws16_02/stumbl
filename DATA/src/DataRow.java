import java.sql.Date;

/**
 * POJO for storing the Data of a Person.
 * No setters provided as we do not need to set the data anywhere else than
 * in the constructor
 * Created by regrau on 22.12.16.
 */
public class DataRow {
    private String name;
    private String birthName;
    private String surName;
    private Date birthDay;
    private String street;
    private int streetNumber;
    private String area;
    private String[] deportationArea;
    private Date[] deportationDay;
    private String deathArea;
    private Date deathDate;

    public DataRow(String name, String birthName, String surName,Date birthDay, String street, String area, String[] deportationArea, Date[] deportationDay, String deathArea, Date deathDate) {
        this.name = name;
        this.birthName = birthName;
        this.surName = surName;
        this.birthDay = birthDay;
        this.street = street;
        this.area = area;
        this.deportationArea = deportationArea;
        this.deportationDay = deportationDay;
        this.deathArea = deathArea;
        this.deathDate = deathDate;
    }

    public String getName() {
        return name;
    }

    public String getBirthName() {
        return birthName;
    }

    public String getSurName() {
        return surName;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public String getStreet() {
        return street;
    }

    public String getArea() {
        return area;
    }

    public String[] getDeportationArea() {
        return deportationArea;
    }

    public Date[] getDeportationDay() {
        return deportationDay;
    }

    public String getDeathArea() {
        return deathArea;
    }

    public Date getDeathDate() {
        return deathDate;
    }
}
