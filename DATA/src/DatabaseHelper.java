import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;


/**
 * Created by regrau on 05.12.16.
 */

public class DatabaseHelper implements DatabaseInterface {
    private final String pathToDatabaseFile= "/home/regrau/Documents/Uni/TechMob/stumbl/DATA/stumbl.db";
    /**
     * This method helps to avoid repeating the connection code in all CRUD methods.
     * If you happen to write a method for handling the data remember to always close the connection after you opened it.
     * Not Threadsafe !!!
     * @return returns the connection to the database.
     */
    private Connection connectDB() {
        Connection connection = null;
        PreparedStatement statement;
        String foreignKeyCheckEnable = "PRAGMA foreign_keys = ON";
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:"+ pathToDatabaseFile);
            statement = connection.prepareStatement(foreignKeyCheckEnable);
            statement.execute();
        } catch (Exception e) {
            System.err.println("Something went wrong while connecting to the database");
            e.printStackTrace();
        }
        return connection;
    }

    public void insertAddress(DataRow row) {
        final String insert = "INSERT INTO Address (Street, Streetnumber) VALUES (?, ?);";
        PreparedStatement statement;
        try {
            Connection connection = connectDB();
            statement = connection.prepareStatement(insert);
            statement.setString(1, row.getStreet());
//            statement.setInt(2, row.getStreetNumber());
//            statement.setInt(3, AreaZipcode); // Needs to be reconsidered
            statement.execute();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void insertDeporation(DataRow row) {
    }

    @Override
    public void insertPerson(DataRow row) {
        final String insert = "INSERT INTO Person" +
                "(Name, Birthname, Surname, Birthday, Death, place_of_death, Addressid)" +
                "VALUES " +
                "(?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement statement;
        try{
            Connection connection = connectDB();
            statement = connection.prepareStatement(insert);
            statement.setString(1, row.getName());
            statement.setString(2, row.getBirthName());
            statement.setString(3, row.getSurName());
            statement.setDate(4, row.getBirthDay());
            statement.setDate(5,row.getDeathDate());
            statement.setString(6,row.getDeathArea());
//            statement.setInt(7,row.getAddressID); //TODO Method to get the AddressID
            statement.execute();
            connection.close();
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void insertCoordinates(DataRow row) {

    }

    @Override
    public void insertDeportationsziel(DataRow row) {

    }

    @Override
    public void insertArea(DataRow row) {
        final String insert = "INSERT INTO Area (Area) VALUES (?);";
        PreparedStatement statement;
        try {
            Connection connection = connectDB();
            statement = connection.prepareStatement(insert);
            statement.setString(1, row.getArea());
            statement.execute();
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insertData(DataRow row) {
       final String insert = "INSERT INTO person" +
               "(vorname, " +
               "birthname, " +
               "surname, " +
               "birthday, " +
               "address, " +
               "area, " +
               "deportationday1, " +
               "deportationdestination1," +
               "deportationday2, " +
               "deportationdestination2," +
               "deportationday3, " +
               "deportationdestination3," +
               "deathdate," +
               "deatharea)" +
               "VALUES" +
               "(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
       PreparedStatement statement;
       try {
           Connection connection = connectDB();
           statement = connection.prepareStatement(insert);
           statement.setString(1, row.getName());
           statement.setString(2, row.getBirthName());
           statement.setString(3, row.getSurName());
           statement.setDate(4, row.getBirthDay());
           statement.setString(5, row.getStreet());
           statement.setString(6, row.getArea());
           statement.setDate(7, row.getDeportationDay()[0]);
           statement.setString(8, row.getDeportationArea()[0]);
           statement.setDate(9, row.getDeportationDay()[1]);
           statement.setString(10, row.getDeportationArea()[1]);
           statement.setDate(11, row.getDeportationDay()[2]);
           statement.setString(12, row.getDeportationArea()[2]);
           statement.setDate(13, row.getDeathDate());
           statement.setString(14, row.getDeathArea());
           statement.execute();
       }catch (SQLException e){
           e.printStackTrace();
       }
    }
}

