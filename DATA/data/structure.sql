CREATE TABLE "person"(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  vorname varchar,
  birthname varchar,
  surname varchar,
  birthday date,
  address varchar,
  area varchar,
  deportationday1 date,
  deportationdestination1 varchar,
  deportationday2 date,
  deportationdestination2 varchar,
  deportationday3 date,
  deportationdestination3 varchar,
  deathdate date,
  deatharea varchar
);

