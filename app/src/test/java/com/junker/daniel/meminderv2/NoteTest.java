package com.junker.daniel.meminderv2;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by rezapurnamaa on 21/01/2017.
 */
public class NoteTest {

    private Note note;
    private Note.Category categoryPersonal = Note.Category.PERSONAL;


    @Before
    public void setUp() throws Exception {
        note = new Note("title", "message", 1, 1, categoryPersonal);

    }

    @After
    public void tearDown() throws Exception {
        note = null;
    }

    @Test
    public void getTitle() throws Exception {
        String expected = "title";
        assertEquals(expected, note.getTitle());
    }

    @Test
    public void getCategory() throws Exception {
        Note.Category expectedCategory = Note.Category.PERSONAL;
        assertEquals(expectedCategory, note.getCategory());
    }

    @Test
    public void getMessage() throws Exception {
        String expectedMessage = "message";
        assertEquals(expectedMessage, note.getMessage());

    }

    @Test
    public void getNoteId() throws Exception {
        int expectedId = 1;
        assertEquals(expectedId, note.getNoteId());

    }

    @Test
    public void getDateCreatedMilli() throws Exception {
        int expectedDateCreatedInMilli = 1;
        assertEquals(expectedDateCreatedInMilli, note.getDateCreatedMilli());
    }
}