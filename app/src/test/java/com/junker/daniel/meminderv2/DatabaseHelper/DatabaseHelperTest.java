package com.junker.daniel.meminderv2.DatabaseHelper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.junker.daniel.meminderv2.BuildConfig;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static org.junit.Assert.*;

/**
 * Created by rezapurnamaa on 24/01/2017.
 */

@RunWith(RobolectricGradleTestRunner.class)
// To use Robolectric you'll need to setup some constants.
// Change it according to your needs.
@Config(constants = BuildConfig.class, sdk = 21, manifest = "/src/main/AndroidManifest.xml")
public class DatabaseHelperTest {
    private Context context;
    private Cursor cursor;
    private DatabaseHelper databaseHelper;
    private SQLiteDatabase db;

    @Before
    public void setUp() throws Exception {
        context = RuntimeEnvironment.application;
        databaseHelper = new DatabaseHelper(context);
        db = databaseHelper.getWritableDatabase();

    }

    @After
    public void tearDown() throws Exception {
        db.close();
    }

    @Test
    public void testDBCreated() throws Exception {
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        SQLiteDatabase db = databaseHelper.getReadableDatabase();
        assertTrue("db is not opened.", db.isOpen());
        db.close();
    }

    @Test
    public void getPersons() throws Exception {
        cursor = databaseHelper.getPersons();
        assertNotNull(cursor);
        String[] columns = cursor.getColumnNames();
        assertEquals("id", columns[1]);
        assertEquals("Vorname", columns[2]);
        assertEquals("Geburtsname", columns[3]);
        assertEquals("Nachname", columns[4]);
        assertEquals("Geburtstag", columns[5]);
        assertEquals("Adresse", columns[6]);
        cursor.close();
    }

    @Test
    public void searchForPersonName() throws Exception {
        String hedwig = "";
        cursor = databaseHelper.getPersons();
        cursor.moveToFirst();
        if(cursor.moveToFirst()) {
            int i = 0;
            do {
                hedwig = cursor.getString(i);
                //cursor.getColumnIndex("String") throws IndexOutOfBound

                i++;
            }while (!hedwig.equals("Hedwig"));
        }
        assertEquals("Hedwig", hedwig);

        cursor.close();
    }

    //searchZipcode() funktioniert nicht

//    @Test
//    public void searchAddresse() throws Exception {
//        SQLiteDatabase db = databaseHelper.getReadableDatabase();
//        String query = "SELECT rowid as _id, Nachname FROM stolpersteine WHERE Nachname like '%?%'";
//        String abraham = "";
//        cursor = db.rawQuery(query,new String[]{abraham});
//    }

}