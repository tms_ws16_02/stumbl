package com.junker.daniel.meminderv2;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by danieljunker on 10.11.16.
 */

public class Note implements Parcelable {
    /**
     *
     */
    private String title, message;
    /**
     *
     */
    private long noteId, dateCreatedMilli;
    /**
     *
     */
    private Category category;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {

    }


    /**
     *
     */
    public enum Category { PERSONAL, TECHNICAL, QUOTE, FINANCE }

    /**
     *
     * @param title
     * @param message
     * @param noteId
     * @param dateCreatedMilli
     * @param category
     */
    public Note(final String title,
                final String message,
                final long noteId,
                final long dateCreatedMilli,
                final Category category) {
        this.title = title;
        this.message = message;
        this.noteId = noteId;
        this.dateCreatedMilli = dateCreatedMilli;
        this.category = category;
    }

    /**
     *
     * @param title
     * @param message
     * @param category
     */
    public Note(final String title, final String message, final Category category) {
        this.title = title;
        this.message = message;
        this.category = category;
        this.noteId = 0;
        this.dateCreatedMilli = 0;
    }

    public String getTitle() {
        return title;
    }

    public Category getCategory() {
        return category;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "Note{" +
                "title='" + title + '\'' +
                ", message='" + message + '\'' +
                ", noteId=" + noteId +
                ", dateCreatedMilli=" + dateCreatedMilli +
                ", category=" + category +
                '}';
    }

    public long getNoteId() {
        return noteId;
    }

    public long getDateCreatedMilli() {
        return dateCreatedMilli;
    }

    public int getAssociatedDrawable() {
        return categoryToDrawable(category);
    }

    public static int categoryToDrawable(Category noteCategory) {

        switch (noteCategory) {
            case PERSONAL:
                return R.drawable.contacts_img;
            case TECHNICAL:
                return R.drawable.contacts_img;
            case QUOTE:
                return R.drawable.contacts_img;
            case FINANCE:
                return R.drawable.contacts_img;
            default:
                return 0;
        }
    }

}
