package com.junker.daniel.meminderv2;


import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.junker.daniel.meminderv2.DatabaseHelper.DatabaseHelper;


/**
 * A simple {@link ListFragment} subclass.
 */
public class PersonListFragment extends ListFragment {
    private OnHeadlineSelectedListener mCallback;

    public static  int personId;

    // Container Activity must implement this interface
    public interface OnHeadlineSelectedListener {
        void onArticleSelected(int position);
    }

    /**
     *
     */
    private Cursor persons;
    /**
     *
     */
    private PersonNodeAdapter personNodeAdapter;

    @Override
    public View onCreateView(final LayoutInflater inflater,
                             final ViewGroup viewGroup,
                             final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_main, viewGroup, false);
        DatabaseHelper helper = new DatabaseHelper(getActivity());
        persons = helper.getPersons();

        personNodeAdapter = new PersonNodeAdapter(getActivity(), persons);
        this.setListAdapter(personNodeAdapter);
        return view;
    }



    @Override
    public void onListItemClick(final ListView l,
                                final View v,
                                final int position,
                                final long id) {
        super.onListItemClick(l, v, position, id);

        mCallback.onArticleSelected(position);
        personId = position;
        personId++;

        // Set the item as checked to be highlighted when in two-pane layout
        getListView().setItemChecked(position, true);

    }



    @Override
    public void onStart() {
        super.onStart();

        // When in two-pane layout,
        // set the listview to highlight the selected list item
        // (We do this during onStart because at the point the
        // listview is available.)
        if (getFragmentManager().findFragmentById(R.id.realtabhost) != null) {
            getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        }
    }

    @Deprecated
    @Override
    public void onAttach(final Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            mCallback = (OnHeadlineSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }
}
