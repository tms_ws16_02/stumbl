package com.junker.daniel.meminderv2;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by danieljunker on 10.11.16.
 */

public class PersonNodeAdapter extends CursorAdapter {

    public PersonNodeAdapter(final Context context,
                             final Cursor cursor) {
        super(context, cursor, 0);
    }

    @Override
    public View newView(final Context context, final Cursor cursor, final ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.list_row, parent, false);
        bindView(v, context, cursor);
        return v;
    }

    @Override
    public void bindView(final View view, final Context context, final Cursor cursor) {
        TextView textView = (TextView) view.findViewById(R.id.titleHeadlineRow);
        TextView textView1 = (TextView)  view.findViewById(R.id.address);
        TextView textView2 = (TextView)  view.findViewById(R.id.deathDate);
        ImageView imageView = (ImageView) view.findViewById(R.id.listItemNoteImg);

        String name = cursor.getString(cursor.getColumnIndexOrThrow("Vorname"));
        String address = cursor.getString(cursor.getColumnIndexOrThrow("Adresse"));
        String deathDate = cursor.getString(cursor.getColumnIndexOrThrow("_id"));
        textView.setText(name);
        textView1.setText(address);
        textView2.setText(deathDate);
        imageView.setImageResource(R.drawable.stolperstein);
    }
}
