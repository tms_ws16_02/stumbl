package com.junker.daniel.meminderv2;

import android.database.Cursor;


/**
 * Created by danieljunker on 10.11.16.
 */

public class PersonNode {
    private int id, zipcode;
    private String name, birthName, surName, street, area, deathArea, birthday, deathDay;
    private String[] deportationArea, deportationDay;

    public PersonNode(Cursor person) {
        this.id = person.getInt(person.getColumnIndex("id"));
        this.name = person.getString(person.getColumnIndex("name"));
        this.birthName = person.getString(person.getColumnIndex("birthname"));
        this.surName = person.getString(person.getColumnIndex("surname"));
        this.birthday = person.getString(person.getColumnIndex("birthday"));
        this.street = person.getString(person.getColumnIndex("street"));
        this.area = person.getString(person.getColumnIndex("area"));
        this.zipcode = person.getInt(person.getColumnIndex("zipcode"));
        this.deportationDay = new String[3];
        this.deportationArea = new String[3];
        for (int i = 1; i <= 3; i++) {
            this.deportationDay[i - 1] =
                    person.getString(person.getColumnIndex(i + "deportationday"));
            this.deportationArea[i - 1] =
                    person.getString(person.getColumnIndex(i + "deportationarea"));
        }
        this.deportationArea[0] = "somestring";
        this.deathDay = person.getString(person.getColumnIndex("deathday"));
        this.deathArea = person.getString(person.getColumnIndex("deatharea"));
    }

    public int getId() {
        return id;
    }

    public int getZipcode() {
        return zipcode;
    }

    public String getName() {
        return name;
    }

    public String getBirthName() {
        return birthName;
    }

    public String getSurName() {
        return surName;
    }

    public String getStreet() {
        return street;
    }

    public String getArea() {
        return area;
    }

    public String getDeathArea() {
        return deathArea;
    }

    public String getBirthday() {
        return birthday;
    }

    public String getDeathDay() {
        return deathDay;
    }

    public String[] getDeportationArea() {
        return deportationArea;
    }

    public String[] getDeportationDay() {
        return deportationDay;
    }
}
