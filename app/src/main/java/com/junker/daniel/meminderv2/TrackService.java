package com.junker.daniel.meminderv2;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;

/**
 * Created by danieljunker on 17.10.16.
 */

public class TrackService extends Service {

    /**
     *
     */
    private static final int MIN_SECONDS = 60;

    /**
     *
     */
    private static final float MIN_METRES = 7;

    /**
     *
     */
    private LocationManager mgr;

    /**
     *
     */
    private LocationListener listenerCoarse, listenerFine;

    /**
     *
     */
    private String preferredProvider;

    /**
     *
     */
    private static final int LOCATION_ACCURACY = 500;

    /**
     *
     */
    private static final int GPS_TIMEUPDATE = 1500;

    /**
     *
     */
    private static final int GPS_DISTANCEUPDATE = 7;

    /**
     *
     * @param intent
     * @return
     */
    @Nullable
    @Override
    public IBinder onBind(final Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(final Intent intent, final int flags, final int startId) {
        initGPS();
        if (preferredProvider != null) {

            return START_STICKY;
        }
        return START_NOT_STICKY;
    }

    /**
     *
     */
    private void initGPS() {
        mgr = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria fine = new Criteria();
        fine.setAccuracy(Criteria.ACCURACY_FINE);
        fine.setAltitudeRequired(false);
        fine.setBearingRequired(false);
        fine.setSpeedRequired(true);
        fine.setCostAllowed(true);

        Criteria coarse = new Criteria();
        coarse.setAccuracy(Criteria.ACCURACY_COARSE);

        fine.setPowerRequirement(Criteria.POWER_HIGH);
        coarse.setPowerRequirement(Criteria.POWER_LOW);

        String provider = mgr.getBestProvider(coarse, true);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = mgr.getLastKnownLocation(provider);


        if (listenerFine == null || listenerCoarse == null) {
            createLocationListeners();
        }
        if (listenerCoarse != null) {
            mgr.requestLocationUpdates(
                    mgr.getBestProvider(coarse, true),
                    GPS_TIMEUPDATE,
                    GPS_DISTANCEUPDATE,
                    listenerCoarse
            );
        }

        if (listenerFine != null) {
            mgr.requestLocationUpdates(
                    mgr.getBestProvider(fine, true),
                    GPS_TIMEUPDATE,
                    GPS_DISTANCEUPDATE,
                    listenerFine
            );
        }

    }

    @Override
    public boolean onUnbind(final Intent intent) {
        return super.onUnbind(intent);
    }

    /**
     *   Creates LocationListeners
     */
    private void createLocationListeners() {

        listenerFine = new LocationListener() {
            public void onStatusChanged(final String provider,
                                        final int status,
                                        final Bundle extras) {
            }

            public void onProviderEnabled(final String provider) {
            }

            public void onProviderDisabled(final String provider) {
            }

            public void onLocationChanged(final Location location) {
                if (location.getAccuracy() > LOCATION_ACCURACY && location.hasAccuracy()) {
                } else {
                }
            }
        };

        listenerCoarse = new LocationListener() {
            public void onStatusChanged(final String provider,
                                        final int status,
                                        final Bundle extras) {
            }

            public void onProviderEnabled(final String provider) {
            }

            public void onProviderDisabled(final String provider) {
            }

            public void onLocationChanged(final Location location) {
                if (location.getAccuracy() < LOCATION_ACCURACY && location.hasAccuracy()) {

                    if (ActivityCompat.checkSelfPermission(getApplicationContext(),
                            android.Manifest.permission.ACCESS_FINE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED
                            &&
                            ActivityCompat.checkSelfPermission(
                                getApplicationContext(),
                                android.Manifest.permission.ACCESS_COARSE_LOCATION)
                                != PackageManager.PERMISSION_GRANTED) {
                        // ActivityCompat#requestPermissions
                        // request the missing permissions, and then overriding
                        //public void onRequestPermissionsResult(int requestCode
                        // String[] permissions,int[] grantResults)
                        // handle the case where the user grants permissions
                        // See the documentation
                        //for ActivityCompat#requestPermissions for more details
                        return;
                    }
                    mgr.removeUpdates(listenerCoarse);
                }
            }
        };
    }
}
