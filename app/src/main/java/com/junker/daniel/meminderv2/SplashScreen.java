
package com.junker.daniel.meminderv2;

/**
 * Created by danieljunker on 07.10.16.
 */

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;


/**
 *
 */
public class SplashScreen extends Activity {
        /*implements SimpleGestureListener */
    /**
     * CONST_SLEEP - defines HOW long will the sleep period be.
     */
    private static final int CONST_SLEEP = 100;
    /**
     *
     */
    private final float scaleFactor = (float) 0.7;
    /**
     * CONST_INTERVALL - should be the same time like the sleep interval.
     * Gets added to the timer variable.
     */
    private static final int CONST_INTERVALL = 100;
    /**
     *
     */
    private ImageButton btnStart;
    /**
     *
     */
    private GeoLocation geolocation;

    /**
     *
     */
    private ProgressBar progressBar;
    /** Called when the activity is first created. */
    private Thread splashTread;

    /**
     *
     */
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            Intent intent = new Intent(SplashScreen.this,
                    StartActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            SplashScreen.this.finish();
        }
    };
    private TextView tvAuthor;

    /**
     *
     */
    public final void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);
    }

    @Override
    public final void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_splashscreen);
        BitmapFactory.Options options = new BitmapFactory.Options();

        progressBar = (ProgressBar) findViewById(R.id.smoothProgressBar);

         tvAuthor = (TextView) findViewById(R.id.createdby);
        tvAuthor.setText(getString(R.string.authors)+"");
        btnStart = (ImageButton) findViewById(R.id.btnStartXML);
        options.outHeight = btnStart.getHeight();
        options.outWidth = btnStart.getWidth();
        Bitmap bm = BitmapFactory.decodeResource(getResources(),
                R.drawable.quantum_ic_play_arrow_white_24, options);

        btnStart.setImageBitmap(bm);


        //@TODO Add Permission request Handling
        startAnimations();
        btnStart.setOnClickListener(onClickListener);



    }

    /**
     *
     */
    private void startAnimations() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.alpha);
        anim.reset();
        ConstraintLayout l = (ConstraintLayout) findViewById(R.id.lin_lay);
        l.clearAnimation();
        l.startAnimation(anim);
        ImageView iv = (ImageView) findViewById(R.id.splash);
        iv.setScaleX(scaleFactor);
        iv.setScaleY(scaleFactor);
        iv.clearAnimation();
        iv.startAnimation(anim);
        anim = AnimationUtils.loadAnimation(this, R.anim.pulse);

        anim.setRepeatCount(Animation.INFINITE);
        anim.reset();
        iv.clearAnimation();
        iv.startAnimation(anim);
        String uri =  getString(R.string.logoSplash);
        iv.setImageResource(R.drawable.ic_logo);

        //Splashthread, point where the loop starts until killed.
        splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    boolean waited = false;
// Splash screen pause time
                    while (!waited) {
                        sleep(CONST_SLEEP);

                        //TODO:Start Async task for DB & service here. Future Implementation
                    }

                } catch (InterruptedException e) {
// do nothing
                } finally { // Finally block always executes, when try exists.
                    SplashScreen.this.finish();
                }
            }
        };
        splashTread.start();
    }

}

