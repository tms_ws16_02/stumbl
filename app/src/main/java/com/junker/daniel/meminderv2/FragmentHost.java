/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.junker.daniel.meminderv2;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 *
 */
public class FragmentHost extends Fragment {

    static final  String ARG_POSITION = "position";
    private int mCurrentPosition = -1;

    private FragmentTabHost tabHost;

    public FragmentHost() {
    }



    /**
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {

        if (savedInstanceState != null) {
            mCurrentPosition = savedInstanceState.getInt(ARG_POSITION);
        }


        tabHost = new FragmentTabHost(getActivity());
        tabHost.setup(getActivity(), getChildFragmentManager(), R.id.tab_host);

        Bundle mapBundle = new Bundle();
        mapBundle.putInt("Map", 1);
        tabHost.addTab(tabHost.newTabSpec("Tab1").setIndicator("Map"),
                MapTabFragment.class, mapBundle);
        Bundle webViewBundle = new Bundle();
        webViewBundle.putInt("DetailsWeb", 2);
        tabHost.addTab(tabHost.newTabSpec("Tab2").setIndicator("Details"),
                WebViewFragment.class, webViewBundle);

        // Inflate the layout for this fragment
        return tabHost;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        tabHost = null;
    }



    public void updateTextView(int position) {
        mCurrentPosition = position;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Save the current article selection in case we need to recreate the fragment
        outState.putInt(ARG_POSITION, mCurrentPosition);
    }
}




