package com.junker.daniel.meminderv2;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import static com.junker.daniel.meminderv2.FragmentHost.ARG_POSITION;

/**
 * Created by danieljunker on 22.01.17.
 * Show description of Stumble stone in WebView
 */

public class WebViewFragment extends Fragment {
    private int mCurrentPosition;
    private WebView mWebview;
    private String url = "http://www.stolpersteine-berlin.de/de/biografie/";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.web_view_fragment, container, false);
        if (savedInstanceState != null) {
            mCurrentPosition = savedInstanceState.getInt(ARG_POSITION);
        }
//        DatabaseHelper helper = new DatabaseHelper(getActivity());
     //   person = helper.getPersons();

//        person = ;

    /*    tvOne = (TextView) view.findViewById(R.id.tv_one);
        tvTwo = (TextView) view.findViewById(R.id.tv_two);
        tvTwo.setText("horst");
        tvOne.setText("horst");*/

        mWebview = (WebView)view.findViewById(R.id.show_web_view);
        mWebview.loadUrl(url + PersonListFragment.personId);
//        mWebview.loadUrl(url + mCurrentPosition);

        // Enable Javascript
        WebSettings webSettings = mWebview.getSettings();
        webSettings.setJavaScriptEnabled(true);

        // Force links and redirects to open in the WebView instead of a browser
        mWebview.setWebViewClient(new WebViewClient());

        return view;
    }
//
//    public int getZipcode(Cursor cursor) {
//        int urlId = cursor.getColumnIndex("_id");
//        return urlId;
//    }
}
