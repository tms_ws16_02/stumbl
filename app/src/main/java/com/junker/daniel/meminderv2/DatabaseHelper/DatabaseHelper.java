package com.junker.daniel.meminderv2.DatabaseHelper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

/**
 * Created by regrau on 19.01.17.
 */

public class DatabaseHelper extends SQLiteAssetHelper {
    private static final String DB_NAME = "stumbl.db";

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, 1);
    }

    public Cursor getPersons() {
        SQLiteDatabase db = getReadableDatabase();
        return db.rawQuery("SELECT rowid as _id, * FROM stolpersteine", null);
    }

    public Cursor searchForZipcode(String zipcode) {
        SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        String query = "SELECT rowid as _id, zipcode FROM stolpersteine WHERE zipcode like '%?%'";
        return db.rawQuery(query, new String[]{zipcode});
    }
}

