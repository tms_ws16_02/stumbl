#include <jni.h>
#include <string>

extern "C"
jstring
Java_com_junker_daniel_meminderv2_start_Activity_stringFromJNI(
        JNIEnv* env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}
