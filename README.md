# Stumbl
This app helps distributing the idea of [stumblstones](http://www.stolpersteine-berlin.de/en)
It locates the stumblstones near you so you can navigate to them.

# Getting started
To get this project you just clone this repository via
```git
git clone https://some/repo/placeholder/till/itispublic
```
after that you just open this project in Android Studio and start it.
On opening the project you land in the `master` branch which is used as the
developing branch.  
If you want to check the actual release just checkout the release branch.
```git
git checkout release
```
It is tagged so you can switch between different versions.

# Using the app
For now you have to set the app permissions manually.
For that just go to `Settings>Applications>Applicationmanager>stumbl>permissions`
on your Phone

# Authors
* **Daniel Junker**
* **Georg Grauberger**
* **Reza Purnama Arief**
